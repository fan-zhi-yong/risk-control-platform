package com.fan.flink.evaluate;

import com.fan.flink.entity.*;

/**
 * 位移速度评估
 */
public class SpeedEvaluate extends AbstractEvaluate{
    private int thresholdSpeed;

    public SpeedEvaluate(int thresholdSpeed) {
        this.thresholdSpeed = thresholdSpeed;
    }

    @Override
    public void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain) {
        evaluateReport.setMap(EvaluateRisk.SPEDD,doEval(evaluateData,historyData));
    }

    /**
     * 评估操作：判断位移速度是否有风险
     * @param evaluateData 评估数据
     * @param historyData 历史数据
     * @return 是否有风险
     */
    private Boolean doEval(EvaluateData evaluateData, HistoryData historyData) {
        if(historyData == null){
            return false;
        }else {
            double time = (evaluateData.getTime() - historyData.getLastLoginSuccessTime())/1000/60/60; //小时为单位

            double distance = calDistance(evaluateData.getGeoPoint(),historyData.getLastLoginSuccessGeoPoint());
            double speed = distance/time;
            return speed>=thresholdSpeed;
        }
    }

    /**
     * 计算两点之间的球面距离
     * @param geoPoint 地理位置 包含经纬度
     * @param lastLoginSuccessGeoPoint
     * @return 两点之间的球面距离
     */
    private static double calDistance(GeoPoint geoPoint, GeoPoint lastLoginSuccessGeoPoint) {
        //d(x1,y1,x2,y2)=r*arccos(sin(x1)*sin(x2)+cos(x1)*cos(x2)*cos(y1-y2))
        final double r = 6371; //地球半径

        double longitude1 = geoPoint.getLongitude(); //经度
        double latitude1 = geoPoint.getLatitude(); //纬度
        double longitude2 = lastLoginSuccessGeoPoint.getLongitude(); //经度
        double latitude2 = lastLoginSuccessGeoPoint.getLatitude(); //纬度

        double y1 = Math.toRadians(longitude1);
        double x1 = Math.toRadians(latitude1);
        double y2 = Math.toRadians(longitude2);
        double x2 = Math.toRadians(latitude2);

        return r*Math.acos(Math.sin(x1)*Math.sin(x2)+Math.cos(x1)*Math.cos(x2)*Math.cos(y1-y2));

    }

//    public static void main(String[] args) {
//        GeoPoint zb = new GeoPoint(112.448345,38.015509);
//        GeoPoint bj = new GeoPoint(116.4,39.9);
//        double distance = calDistance(zb, bj);
//        System.out.println(distance);
//    }
}
