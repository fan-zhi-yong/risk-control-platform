package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.HistoryData;

/**
 * 所有评估因子的父类型
 */
public abstract class AbstractEvaluate {
    /**
     * 评估方法：就是根据评估数据，结合历史数据，生成一个评估因子的值放入到评估报告里边
     * @param evaluateData
     * @param historyData
     * @param evaluateReport
     * @param evaluateChain 评估链
     */
    public abstract void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain);
}
