package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.EvaluateRisk;
import com.fan.flink.entity.HistoryData;

import java.util.Set;

/**
 * 登录地评估
 */
public class CityEvaluate extends AbstractEvaluate{
    @Override
    public void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain) {
        //做登录地评估，获取到一个Boolean类型的值
        if(historyData == null){
            evaluateReport.setMap(EvaluateRisk.CITY,false);
        }else {
            Set<String> set = historyData.getCities();
            evaluateReport.setMap(EvaluateRisk.CITY,!set.contains(evaluateData.getCity()));
        }

        //放行,交个责任链处理
        evaluateChain.doEvaluate(evaluateData,historyData,evaluateReport);
    }
}
