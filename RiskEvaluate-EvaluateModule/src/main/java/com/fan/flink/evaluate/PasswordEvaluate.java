package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.EvaluateRisk;
import com.fan.flink.entity.HistoryData;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 乱序密码评估
 */
public class PasswordEvaluate extends AbstractEvaluate{
    private final double threshold;

    public PasswordEvaluate(double threshold) {
        this.threshold = threshold;
    }

    @Override
    public void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain) {
        evaluateReport.setMap(EvaluateRisk.PASSWORD,doEval(evaluateData,historyData));


        evaluateChain.doEvaluate(evaluateData,historyData,evaluateReport);
    }

    /**
     * 乱序密码评估是否有风险
     * @param evaluateData
     * @param historyData
     * @return
     */
    private Boolean doEval(EvaluateData evaluateData, HistoryData historyData) {
        if(historyData == null){
            return false;
        }else {
            //计算评估密码对应的向量，和历次成功登录的所有密码的向量
            Set<String> allPassword = historyData.getOrdernessPasswords();
            String ordernessPassword = evaluateData.getOrdernessPassword();
            Set<Character> set = new HashSet<>();
            for (String s : allPassword) {
                for (char c : s.toCharArray()) {
                    set.add(c);
                }
            }
            for (char c : ordernessPassword.toCharArray()) {
                set.add(c);
            }
            List<Character> wordBag = set.stream().sorted().collect(Collectors.toList());

            //计算向量
            int[] xiangLiang = calcXiangLiang(ordernessPassword, wordBag);

            //计算历次成功登录的密码向量
            List<int[]> list = allPassword.stream().map(password -> calcXiangLiang(password, wordBag)).collect(Collectors.toList());

            //把list中的每一个向量和xiangLiang进行余弦相似性的计算
            List<Double> collect = list.stream().map(xl -> calcSimilarity(xl, xiangLiang)).collect(Collectors.toList());

            //通过流计算中的filter算子过滤
            List<Double> list2 = collect.stream().filter(si -> si > threshold).collect(Collectors.toList());
            return list2.size() == 0;
        }
    }

    /**
     * 计算乱序密码对应的向量
     * @param ordernessPassword 乱序密码
     * @param list 词袋库
     * @return 乱序密码的向量
     */
    private int[] calcXiangLiang(String ordernessPassword, List<Character> list){
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            Character c = list.get(i);
            int count = 0;

            char[] chars = ordernessPassword.toCharArray();
            for (char ch : chars) {
                if(c == ch){
                    count ++;
                }
            }
            array[i] = count;
        }

        return array;
    }

    /**
     * 计算两个向量的余弦相似性
     * @param a 一个向量
     * @param b 另一个向量
     * @return 两个向量的余弦相似性
     */
    private double calcSimilarity(int[] a,int[] b){
        //计算分子
        double fenZi = 0;
        for (int i = 0;i<a.length;i++){
            fenZi = a[i]+b[i];
        }
        //计算分母
        double sumA = 0;
        double sumB = 0;
        for (int ai : a){
            sumA += ai*ai;
        }
        for (int bi : b){
            sumB += Math.pow(bi,2);
        }
        double fenMu = Math.sqrt(sumA) * Math.sqrt(sumB);
        return fenZi / fenMu;
    }
}
