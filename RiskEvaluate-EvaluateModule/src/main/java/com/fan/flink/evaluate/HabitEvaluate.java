package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.EvaluateRisk;
import com.fan.flink.entity.HistoryData;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 登录习惯评估
 */
public class HabitEvaluate extends AbstractEvaluate{
    private int thresholdCount; //规定阈值

    public HabitEvaluate(int thresholdCount) {
        this.thresholdCount = thresholdCount;
    }

    @Override
    public void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain) {
        //1.评估，最后获取到一个boolean类型评估
        evaluateReport.setMap(EvaluateRisk.HABIT,doHabitEvaluate(evaluateData,historyData));

        //2.放行，把执行控制权交给责任维护链

    }

    /**
     * 执行登录习惯的评估
     * @param evaluateData 评估数据
     * @param historyData 历史数据
     * @return 登录习惯是否有风险
     */
    private Boolean doHabitEvaluate(EvaluateData evaluateData, HistoryData historyData) {
        if(historyData == null){
            return false;
        }else {
            //累计登录次数没有达到规定次数
            //计算累计登录次数
            Map<String, Map<String, Integer>> map = historyData.getHabitsData();
            Collection<Map<String, Integer>> values = map.values();
            Stream<Map<String, Integer>> stream = values.stream();
            Stream<Collection<Integer>> stream1 = stream.map(map1 -> map1.values());
            Stream<Optional<Integer>> stream2 = stream1.map(coll -> coll.stream().reduce((v1, v2) -> v1 + v2));
            Stream<Integer> stream3 = stream2.map(optional -> optional.get());
            Integer totalCount = stream3.reduce((v1, v2) -> v1 + v2).get();
            if(totalCount<thresholdCount){
                return false;
            }else{
                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(evaluateData.getTime());
//                int week = calendar.get(Calendar.DAY_OF_WEEK);
                String week = calendar.get(Calendar.DAY_OF_WEEK) + "";
                if (map.containsKey(week)){
                    Map<String, Integer> xiaomap = map.get(week);
                    String hour = calendar.get(Calendar.HOUR_OF_DAY)+"";
                    if(xiaomap.containsKey(hour)){
                        Integer count = xiaomap.get(hour);
                        //获得到xiaomap中的所有value,进行升序排序，取三分之二位置出的值作为阈值
                        List<Integer> collect = xiaomap.values().stream().sorted().collect(Collectors.toList());
                        Integer threshold = collect.get(collect.size() * 2 / 3);
                        return count < threshold;
                    }else {
                        return true;
                    }
                }else{
                    return true;
                }
            }

        }

    }
    /*//验证一下Calendar
    public static void main(String[] args) {
        //
        Calendar calendar=Calendar.getInstance();//表示当前时间对应的日历
        //有一个时间戳：2010-10-20 15:20:58这个时间对应的时间戳是：1287559258000
        System.out.println(calendar);

        calendar.setTimeInMillis(1287559258000L);
        //System.out.println(calendar);//
        //需要获取到星期以及小时
        int week = calendar.get(Calendar.DAY_OF_WEEK);//一周的第几天
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        System.out.println(week+"***"+hour);


    }*/
}
