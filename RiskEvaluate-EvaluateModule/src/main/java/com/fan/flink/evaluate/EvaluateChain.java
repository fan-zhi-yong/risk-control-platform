package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.HistoryData;

import java.util.List;

/**
 * 评估链条，把所有的责任串起来
 */
public class EvaluateChain {
    private List<AbstractEvaluate> list; //所有的评估因子
    private int position; //整个链条执行到了哪里

    public EvaluateChain(List<AbstractEvaluate> list) {
        this.list = list;
    }

    public void doEvaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport){
        if (position<list.size()){
            AbstractEvaluate abstractEvaluate = list.get(position);
            position++;
            abstractEvaluate.evaluate(evaluateData,historyData,evaluateReport,this);
        }
    }
}
