package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.EvaluateRisk;
import com.fan.flink.entity.HistoryData;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 输入特征评估
 */
public class InputFeaturesEvaluate extends AbstractEvaluate{
    @Override
    public void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain) {
        evaluateReport.setMap(EvaluateRisk.INPUT_FEATRUES,doEval(evaluateData,historyData));


        evaluateChain.doEvaluate(evaluateData,historyData,evaluateReport);
    }

    /**
     * 计算输入特征是否有风险
     * @param evaluateData
     * @param historyData
     * @return
     */
    private Boolean doEval(EvaluateData evaluateData, HistoryData historyData) {
        if(historyData == null){
            return false;
        }else {
            double[] inputFeatures = evaluateData.getInputFeatures(); //这次评估的输入特征
            List<double[]> list = historyData.getInputFeatures(); //历次成功登录的输入特征

            //计算历次成功登录的输入特征的圆心点
            double[] sum = new double[list.get(0).length];
            for (double[] doubles : list) {
                for (int i = 0; i < doubles.length; i++) {
                    sum[i] += doubles[i];
                }
            }
            //圆心点
            double[] yuanXin = new double[sum.length];
            for (int i = 0; i < sum.length; i++) {
                yuanXin[i] = sum[i] / list.size();
            }
            //计算历次成功登录到圆心的欧氏距离
            List<Double> list2 = list.stream().map(dian -> calcEuclideanDistance(dian, yuanXin)).sorted().collect(Collectors.toList());
            Double threshold = list2.get(list2.size() * 2 / 3);

            //计算评估数据的输入特征到圆心点的欧式距离
            double distance = calcEuclideanDistance(inputFeatures,yuanXin);
            return distance > threshold;
        }
    }

    /**
     * 根据欧式距离公式计算两点之间的欧式距离
     * @param x
     * @param y
     * @return
     */
    private double calcEuclideanDistance(double[] x, double[] y) {
        double sum = 0;
        for (int i = 0; i < x.length; i++) {
            sum += Math.pow((x[i] - y[i]),2);
        }
        return Math.sqrt(sum);
    }
}
