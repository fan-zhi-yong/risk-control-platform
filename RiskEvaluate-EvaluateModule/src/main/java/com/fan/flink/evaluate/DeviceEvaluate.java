package com.fan.flink.evaluate;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.EvaluateRisk;
import com.fan.flink.entity.HistoryData;

import java.util.List;

public class DeviceEvaluate extends AbstractEvaluate {
    @Override
    public void evaluate(EvaluateData evaluateData, HistoryData historyData, EvaluateReport evaluateReport, EvaluateChain evaluateChain) {
        if (historyData == null){
            evaluateReport.setMap(EvaluateRisk.DEVICE,false);
        } else {
            String device = evaluateData.getDevice();
            List<String> devices = historyData.getDevices();
            evaluateReport.setMap(EvaluateRisk.DEVICE,!devices.contains(device));
        }
        //放行
        evaluateChain.doEvaluate(evaluateData,historyData,evaluateReport);
    }
}
