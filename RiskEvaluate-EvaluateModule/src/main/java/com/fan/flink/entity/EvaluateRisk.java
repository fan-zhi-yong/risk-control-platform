package com.fan.flink.entity;

public enum EvaluateRisk {
    CITY(1),
    DEVICE(2),
    PASSWORD(3),
    INPUT_FEATRUES(4),
    SPEDD(5),
    HABIT(6);

    private EvaluateRisk(int order){
        this.order = order;
    }
    private int order;
    public int getOrder() {
        return order;
    }
    EvaluateRisk() {
    }
}
