package com.fan.flink.entity;

import java.util.Arrays;

/**
 * 登录成功的类
 * 登录成功的日志转换成登录成功的对象的类
 */
public class LoginSuccessData {
    private long time; //时间戳
    private String appName; //应用名
    private String username; //用户名
    private String uuid; //登录唯一标记
    private String ordernessPassword; //乱序密码
    private String city; //登录城市
    private GeoPoint geoPoint; //地理位置
    private double[] inputFeatures; //输入特征
    private String device; //设备

    public LoginSuccessData() {
    }

    public LoginSuccessData(long time, String appName, String username, String uuid, String ordernessPassword, String city, GeoPoint geoPoint, double[] inputFeatures, String device) {
        this.time = time;
        this.appName = appName;
        this.username = username;
        this.uuid = uuid;
        this.ordernessPassword = ordernessPassword;
        this.city = city;
        this.geoPoint = geoPoint;
        this.inputFeatures = inputFeatures;
        this.device = device;
    }

    @Override
    public String toString() {
        return "LoginSuccessData{" +
                "time=" + time +
                ", appName='" + appName + '\'' +
                ", username='" + username + '\'' +
                ", uuid='" + uuid + '\'' +
                ", ordernessPassword='" + ordernessPassword + '\'' +
                ", city='" + city + '\'' +
                ", geoPoint=" + geoPoint +
                ", inputFeatures=" + Arrays.toString(inputFeatures) +
                ", device='" + device + '\'' +
                '}';
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOrdernessPassword() {
        return ordernessPassword;
    }

    public void setOrdernessPassword(String ordernessPassword) {
        this.ordernessPassword = ordernessPassword;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public double[] getInputFeatures() {
        return inputFeatures;
    }

    public void setInputFeatures(double[] inputFeatures) {
        this.inputFeatures = inputFeatures;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
