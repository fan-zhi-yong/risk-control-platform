package com.fan.flink.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 历史数据，历次成功登录留存下来的数据，作为生成评估报告的数据依据
 */
public class HistoryData {
    private Set<String> cities; //保存的是历次成功登录的登录地
    private List<String> devices; //保存历次成功登录的设备信息
    private Set<String> ordernessPasswords; //保存历次成功的登录的乱序密码
    private List<double[]> inputFeatures; //保存历次成功的输入特征，只保存最近的
    private long lastLoginSuccessTime; //最后一次成功登录的时间
    private GeoPoint lastLoginSuccessGeoPoint; //最后一次成功登录的地理位置
    // map<星期,map<小时，登录次数>>
    private Map<String,Map<String,Integer>> habitsData;  //存储的登录习惯对应的数据

    public HistoryData() {
    }

    public HistoryData(Set<String> cities, List<String> devices, Set<String> ordernessPasswords, List<double[]> inputFeatures, long lastLoginSuccessTime, GeoPoint lastLoginSuccessGeoPoint, Map<String, Map<String, Integer>> habitsData) {
        this.cities = cities;
        this.devices = devices;
        this.ordernessPasswords = ordernessPasswords;
        this.inputFeatures = inputFeatures;
        this.lastLoginSuccessTime = lastLoginSuccessTime;
        this.lastLoginSuccessGeoPoint = lastLoginSuccessGeoPoint;
        this.habitsData = habitsData;
    }

    public Set<String> getCities() {
        return cities;
    }

    public void setCities(Set<String> cities) {
        this.cities = cities;
    }

    public List<String> getDevices() {
        return devices;
    }

    public void setDevices(List<String> devices) {
        this.devices = devices;
    }

    public Set<String> getOrdernessPasswords() {
        return ordernessPasswords;
    }

    public void setOrdernessPasswords(Set<String> ordernessPasswords) {
        this.ordernessPasswords = ordernessPasswords;
    }

    public List<double[]> getInputFeatures() {
        return inputFeatures;
    }

    public void setInputFeatures(List<double[]> inputFeatures) {
        this.inputFeatures = inputFeatures;
    }

    public long getLastLoginSuccessTime() {
        return lastLoginSuccessTime;
    }

    public void setLastLoginSuccessTime(long lastLoginSuccessTime) {
        this.lastLoginSuccessTime = lastLoginSuccessTime;
    }

    public GeoPoint getLastLoginSuccessGeoPoint() {
        return lastLoginSuccessGeoPoint;
    }

    public void setLastLoginSuccessGeoPoint(GeoPoint lastLoginSuccessGeoPoint) {
        this.lastLoginSuccessGeoPoint = lastLoginSuccessGeoPoint;
    }

    public Map<String, Map<String, Integer>> getHabitsData() {
        return habitsData;
    }

    public void setHabitsData(Map<String, Map<String, Integer>> habitsData) {
        this.habitsData = habitsData;
    }

    @Override
    public String toString() {
        return "HistoryData{" +
                "cities=" + cities +
                ", devices=" + devices +
                ", ordernessPasswords=" + ordernessPasswords +
                ", inputFeatures=" + inputFeatures +
                ", lastLoginSuccessTime=" + lastLoginSuccessTime +
                ", lastLoginSuccessGeoPoint=" + lastLoginSuccessGeoPoint +
                ", habitsData=" + habitsData +
                '}';
    }

}
