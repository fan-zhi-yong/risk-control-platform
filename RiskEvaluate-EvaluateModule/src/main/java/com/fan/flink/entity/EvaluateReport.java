package com.fan.flink.entity;

/**
 * 评估报告类
 * 里边包含了两部分内容，一部分基础信息，一部分评估因子是否有风险
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EvaluateReport {
    private long time; //时间戳
    private String appName; //应用名
    private String city; //登录城市

    private String geoPointStr; //地理位置 经度纬度


    //六个评估因子对应六个boolean类型的值
    private Map<EvaluateRisk,Boolean> map = new HashMap<>();

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGeoPointStr() {
        return geoPointStr;
    }

    public void setGeoPointStr(String geoPointStr) {
        this.geoPointStr = geoPointStr;
    }

    public Map<EvaluateRisk, Boolean> getMap() {
        return map;
    }

    public void setMap(EvaluateRisk evaluateRisk,Boolean isRisk) {
        map.put(evaluateRisk,isRisk);
    }

    public EvaluateReport() {
    }

    public EvaluateReport(long time, String appName, String city, String geoPointStr) {
        this.time = time;
        this.appName = appName;
        this.city = city;
        this.geoPointStr = geoPointStr;
        map.put(EvaluateRisk.CITY,false);
        map.put(EvaluateRisk.DEVICE,false);
        map.put(EvaluateRisk.PASSWORD,false);
        map.put(EvaluateRisk.INPUT_FEATRUES,false);
        map.put(EvaluateRisk.SPEDD,false);
        map.put(EvaluateRisk.HABIT,false);

    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(time);
        buffer.append(" ");
        buffer.append(appName);
        buffer.append(" ");
        buffer.append(city);
        buffer.append(" ");
        buffer.append(geoPointStr);
        buffer.append(" ");

        List<EvaluateRisk> list = map.keySet().stream().sorted((er1, er2) -> {
            if (er1.getOrder() > er2.getOrder())
                return 1;
            else
                return -1;
        }).collect(Collectors.toList());
        String str = list.stream().map(evaluateRisk -> map.get(evaluateRisk))
                .map(v -> v + " ")
                .reduce((v1, v2) -> v1 + " " + v2).get();

        buffer.append(str);

        return buffer.toString();
    }
}
