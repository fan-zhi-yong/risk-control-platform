package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

/**
 * 更新父类
 */
public abstract class AbstractUpdate {
    /**
     * 更新操作
     * @param loginSuccessData 登录成功数据
     * @param historyData      历史数据
     * @param updateChain      更新链
     */
    public abstract void update(LoginSuccessData loginSuccessData, HistoryData historyData, UpdateChain updateChain);
}
