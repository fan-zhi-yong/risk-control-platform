package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

import java.util.HashSet;
import java.util.Set;

public class PasswordUpdate extends AbstractUpdate{
    @Override
    public void update(LoginSuccessData loginSuccessData, HistoryData historyData, UpdateChain updateChain) {
        Set<String> ordernessPasswords = historyData.getOrdernessPasswords();
        String pass = loginSuccessData.getOrdernessPassword();
        if (ordernessPasswords == null){
            ordernessPasswords = new HashSet<>();
            ordernessPasswords.add(pass);
            historyData.setOrdernessPasswords(ordernessPasswords);
        }else {
            ordernessPasswords.add(pass);
        }
        updateChain.doUpdate(loginSuccessData,historyData);
    }
}
