package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

import java.util.List;

/**
 * 更新链
 */
public class UpdateChain {
    private List<AbstractUpdate> list; //所有的更新因子
    private int position; //位置，执行到了链的哪个责任

    public UpdateChain(List<AbstractUpdate> list) {
        this.list = list;
    }

    public void doUpdate(LoginSuccessData loginSuccessData, HistoryData historyData){
        if(position < list.size()){
            AbstractUpdate abstractUpdate = list.get(position);
            position++;
            abstractUpdate.update(loginSuccessData,historyData,this);
        }
    }
}
