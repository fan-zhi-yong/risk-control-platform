package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

import java.util.ArrayList;
import java.util.List;

public class DeviceUpdate extends AbstractUpdate{
    private int count; //只留存最近的count条设备信息

    public DeviceUpdate(int count) {
        this.count = count;
    }

    @Override
    public void update(LoginSuccessData loginSuccessData, HistoryData historyData, UpdateChain updateChain) {
        List<String> devices = historyData.getDevices();
        String device = loginSuccessData.getDevice();
        if(devices == null){
            devices = new ArrayList<>();
            devices.add(device);
            historyData.setDevices(devices);
        }else {
            devices.add(device);
        }
        if (devices.size() > count){
            devices.remove(0); //删除最前面
        }
        updateChain.doUpdate(loginSuccessData,historyData);
    }
}
