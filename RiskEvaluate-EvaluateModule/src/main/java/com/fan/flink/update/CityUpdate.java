package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

import java.util.HashSet;
import java.util.Set;

public class CityUpdate extends AbstractUpdate{
    @Override
    public void update(LoginSuccessData loginSuccessData, HistoryData historyData, UpdateChain updateChain) {
        String city = loginSuccessData.getCity();
        Set<String> cities = historyData.getCities();
        if(cities == null){
            //第一次登录
            cities = new HashSet<>();
            cities.add(city);
            historyData.setCities(cities);
        }else {
            cities.add(city);
        }
        updateChain.doUpdate(loginSuccessData,historyData);
    }
}
