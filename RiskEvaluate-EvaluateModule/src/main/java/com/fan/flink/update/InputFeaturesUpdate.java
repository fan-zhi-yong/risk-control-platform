package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

import java.util.ArrayList;
import java.util.List;

public class InputFeaturesUpdate extends AbstractUpdate{
    private int count;

    public InputFeaturesUpdate(int count) {
        this.count = count;
    }

    @Override
    public void update(LoginSuccessData loginSuccessData, HistoryData historyData, UpdateChain updateChain) {
        double[] inputFeatures = loginSuccessData.getInputFeatures();
        List<double[]> list = historyData.getInputFeatures();
        if (list == null){
            list = new ArrayList<>();
            list.add(inputFeatures);
            historyData.setInputFeatures(list);
        }else {
            list.add(inputFeatures);
        }
        if(list.size() > count){
            list.remove(0);
        }
        updateChain.doUpdate(loginSuccessData,historyData);
    }
}
