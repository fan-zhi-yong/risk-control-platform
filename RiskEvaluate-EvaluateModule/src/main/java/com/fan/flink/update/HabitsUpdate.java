package com.fan.flink.update;

import com.fan.flink.entity.HistoryData;
import com.fan.flink.entity.LoginSuccessData;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class HabitsUpdate extends AbstractUpdate{
    @Override
    public void update(LoginSuccessData loginSuccessData, HistoryData historyData, UpdateChain updateChain) {
        Map<String, Map<String, Integer>> habitsData = historyData.getHabitsData();
        long time = loginSuccessData.getTime();
        Calendar calendar = Calendar.getInstance(); //表示当前时间对应的日历
        calendar.setTimeInMillis(loginSuccessData.getTime()); //评估数据中的登录数据
        String week = calendar.get(Calendar.DAY_OF_WEEK) + "";
        String hour = calendar.get(Calendar.HOUR_OF_DAY) + "";
        HashMap<String, Integer> xiaoMap = new HashMap<>();
        xiaoMap.put(hour,1);
        if (habitsData == null){
            habitsData = new HashMap<>();
            habitsData.put(week,xiaoMap);
            historyData.setHabitsData(habitsData);
        }else {
            if (habitsData.containsKey(week)){
                Map<String, Integer> xiaoMap2 = habitsData.get(week);
                if (xiaoMap2.containsKey(hour)){
                    xiaoMap2.put(hour,xiaoMap2.get(hour)+1);
                }else{
                    xiaoMap2.put(hour,1);
                }
            }else {
                habitsData.put(week,xiaoMap);
            }
        }

        updateChain.doUpdate(loginSuccessData,historyData);
    }
}
