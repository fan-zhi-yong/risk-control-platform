package com.fan.flink.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
    public static void main(String[] args) {
        final String regex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(EVALUATE|SUCCESS)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";
        final String string = "INFO 2020-06-19 10:23:16 WebApplication EVALUATE [zhangsan] 119e76e86bea42e9a098c2461a6b9314 \"123456\" Zhengzhou \"113.65,34.76\" [1500,2800,3100] \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36\"";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            System.out.println("Full match: " + matcher.group(0));

            for (int i = 1; i <= matcher.groupCount(); i++) {
                System.out.println("Group " + i + ": " + matcher.group(i));
            }
        }
    }
}
