package com.fan.flink.util;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.GeoPoint;
import com.fan.flink.entity.LoginSuccessData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 这是一个工具类
 * 1.把评估日志解析乘评估对象
 * 2.把登录日志解析为登录成功对象
 */
public class EvaluateUtil {
    public static EvaluateData parseLog2EvaluateData(String log) {
        final String regex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(EVALUATE)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";


        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(log);

        if (matcher.find()) {
            //matcher.group(1)
            long time = 0;
            String dateStr = matcher.group(1);
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            //matcher.group(8)
            GeoPoint geoPoint = new GeoPoint();
            String geoPointStr = matcher.group(8);
            String[] str = geoPointStr.split(",");
            geoPoint.setLongitude(Double.parseDouble(str[0]));
            geoPoint.setLatitude(Double.parseDouble(str[1]));

            //matcher.group(9)

            String str2 = matcher.group(9);
            String[] split = str2.split(",");

            double[] inputFeartures = new double[split.length];
            for (int i=0;i<split.length;i++) {
                inputFeartures[i] = Double.parseDouble(split[i]);
            }

            return new EvaluateData(time,matcher.group(2),matcher.group(4),matcher.group(5),matcher.group(6),matcher.group(7),geoPoint,inputFeartures,matcher.group(10));
        }
        return null;
    }
    public static LoginSuccessData parseLog2SuccessData(String log){
        final String regex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(SUCCESS)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(log);

        if (matcher.find()) {
            //matcher.group(1)
            long time = 0;
            String dateStr = matcher.group(1);
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            //matcher.group(8)
            GeoPoint geoPoint = new GeoPoint();
            String geoPointStr = matcher.group(8);
            String[] str = geoPointStr.split(",");
            geoPoint.setLongitude(Double.parseDouble(str[0]));
            geoPoint.setLatitude(Double.parseDouble(str[1]));

            //matcher.group(9)

            String str2 = matcher.group(9);
            String[] split = str2.split(",");

            double[] inputFeartures = new double[split.length];
            for (int i=0;i<split.length;i++) {
                inputFeartures[i] = Double.parseDouble(split[i]);
            }

            return new LoginSuccessData(time,matcher.group(2),matcher.group(4),matcher.group(5),matcher.group(6),matcher.group(7),geoPoint,inputFeartures,matcher.group(10));
        }
        return null;
    }

    /**
     * 判断日志是否为登录成功日志
     * @param log
     * @return
     */
    public static boolean isLoginSuccessLog(String log){
        final String regex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(SUCCESS)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(log);
        return matcher.find();
    }

    /**
     * 判断日志是否为评估日志
     * @param log
     * @return
     */
    public static boolean isEvaluateLog(String log){
        final String regex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(EVALUATE)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";


        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(log);
        return matcher.find();
    }

    /**
     * 解析日志生成 应用名：用户名
     * @param log 日志
     * @return 应用名：用户名
     */
    public static String parseLog2AppNameAndUsername(String log){
        final String successRegex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(SUCCESS)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";

        final Pattern successPattern = Pattern.compile(successRegex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher successMatcher = successPattern.matcher(log);
        if(successMatcher.find()){
            return successMatcher.group(2) + ":" + successMatcher.group(4);
        }
        final String regex = "INFO\\s+(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})\\s+([a-z0-9\\u4e00-\\u9fa5]*)\\s+(EVALUATE)\\s+\\[([a-z0-9\\u4e00-\\u9fa5]*)\\]\\s+([a-z0-9]{32})\\s+\\\"([a-z0-9\\.\\-\\,]{6,12})\\\"\\s+([a-z\\u4e00-\\u9fa5]*)\\s+\\\"([0-9\\.\\,]*)\\\"\\s+\\[([0-9\\,\\.]*)\\]\\s+\\\"(.*)\\\"";


        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(log);
        if (matcher.find()){
            return matcher.group(2) + ":" + matcher.group(4);
        }

        return null;
    }

}
