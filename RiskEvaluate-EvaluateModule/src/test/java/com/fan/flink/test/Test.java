package com.fan.flink.test;

import com.fan.flink.entity.EvaluateData;
import com.fan.flink.entity.EvaluateReport;
import com.fan.flink.entity.HistoryData;
import com.fan.flink.evaluate.*;
import com.fan.flink.entity.LoginSuccessData;
import com.fan.flink.update.*;
import com.fan.flink.util.EvaluateUtil;

import java.util.ArrayList;
import java.util.List;

public class Test {
    HistoryData historyData = null;
    @org.junit.Before
    public void testUpdate(){
        String log = "INFO 2020-06-19 10:23:16 WebApplication SUCCESS [zhangsan] 119e76e86bea42e9a098c2461a6b9314 \"123456\" Zhengzhou \"113.65,34.76\" [1500,2800,3100] \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36\"";
        LoginSuccessData loginSuccessData = EvaluateUtil.parseLog2SuccessData(log);
        historyData = new HistoryData();

        List<AbstractUpdate> list = new ArrayList<>();
        list.add(new CityUpdate());
        list.add(new DeviceUpdate(100));
        list.add(new HabitsUpdate());
        list.add(new InputFeaturesUpdate(100));
        list.add(new PasswordUpdate());
        UpdateChain updateChain = new UpdateChain(list);
        updateChain.doUpdate(loginSuccessData,historyData);
        historyData.setLastLoginSuccessGeoPoint(loginSuccessData.getGeoPoint());
        historyData.setLastLoginSuccessTime(loginSuccessData.getTime());

        System.out.println(historyData);
    }
    @org.junit.Test
    public void testEvaluate(){
        String log = "INFO 2020-06-19 10:23:16 WebApplication EVALUATE [zhangsan] 119e76e86bea42e9a098c2461a6b9314 \"abcdef\" beijing \"116.4,39.9\" [15,28,31] \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36\"";
        EvaluateData evaluateData = EvaluateUtil.parseLog2EvaluateData(log);

        //创建一个评估报告模板
        EvaluateReport evaluateReport = new EvaluateReport(evaluateData.getTime(), evaluateData.getAppName(), evaluateData.getCity(), evaluateData.getGeoPoint().getLongitude() + "," + evaluateData.getGeoPoint().getLatitude());

        ArrayList<AbstractEvaluate> list = new ArrayList<>();
        list.add(new CityEvaluate());
        list.add(new DeviceEvaluate());
        list.add(new HabitEvaluate(100));
        list.add(new InputFeaturesEvaluate());
        list.add(new PasswordEvaluate(0.95));
        list.add(new SpeedEvaluate(750));
        EvaluateChain evaluateChain = new EvaluateChain(list);
        evaluateChain.doEvaluate(evaluateData,historyData,evaluateReport);
        System.out.println(evaluateReport);
    }
}
