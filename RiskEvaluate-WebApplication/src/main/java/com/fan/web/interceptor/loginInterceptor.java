package com.fan.web.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import scala.tools.jline_embedded.internal.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;

/**
 * 登录拦截器
 */
@Component
public class loginInterceptor implements HandlerInterceptor {
    private Logger logger = LoggerFactory.getLogger(loginInterceptor.class);
    private String log = "";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log = "WebApplication EVALUATE [zhangsan] 119e76e86bea42e9a098c2461a6b9314 \"123456\" Zhengzhou \"113.65,34.76\" [1500,2800,3100] \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36\"";

        //发送一个评估日志
        logger.info(log);

        //到接口中读取评估报告，如果确定有风险，方法返回false


        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        boolean isLogin = (boolean) request.getAttribute("isLogin");
        if (isLogin){
            logger.info(log.replace("EVALUATE","SUCCESS"));
        }

    }
}
