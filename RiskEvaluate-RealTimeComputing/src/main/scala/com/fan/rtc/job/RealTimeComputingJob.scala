package com.fan.rtc.job

import com.fan.flink.entity.{EvaluateData, EvaluateReport, HistoryData, LoginSuccessData}
import com.fan.flink.evaluate._
import com.fan.flink.update._
import com.fan.flink.util.EvaluateUtil
import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.api.common.serialization.{Encoder, SimpleStringEncoder}
import org.apache.flink.api.common.state.{MapState, MapStateDescriptor, StateTtlConfig, ValueState, ValueStateDescriptor}
import org.apache.flink.api.common.time.Time
import org.apache.flink.configuration.Configuration
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner
import org.apache.flink.streaming.api.scala._
import org.apache.htrace.shaded.fasterxml.jackson.databind
import org.apache.htrace.shaded.fasterxml.jackson.databind.ObjectMapper

import java.util

/**
 * 实时计算
 */
object RealTimeComputingJob {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    //模拟数据，通过socket完成
    val log: DataStream[String] = environment.socketTextStream("hadoop10", 9999)
    //对log进行过滤
    val dataStream: DataStream[String] = log.filter(log => EvaluateUtil.isEvaluateLog(log) || EvaluateUtil.isLoginSuccessLog(log))

    //把dataStream映射成（应用名，用户名，log）的数据结果
    val keyedStream: KeyedStream[(String, String), String] = dataStream
      .map(log => (EvaluateUtil.parseLog2AppNameAndUsername(log), log))
      .keyBy(_._1)
    val result: DataStream[EvaluateReport] = keyedStream.map(new MyMapFunction)

    //评估报告输出，模拟，打印，最后写入hdfs
    result.filter(_!=null).print()

    var basePath:Path=new Path("hdfs://hadoop10:9000/final-project")
    var encoder:Encoder[String] = new SimpleStringEncoder[String]()
    val builder: StreamingFileSink.DefaultRowFormatBuilder[String] = StreamingFileSink.forRowFormat(basePath, encoder)
    val streamingFileSink: StreamingFileSink[String] = builder.build()
    //生成一个文件夹 2023-2-25--15 每小时生成一个
    //如果需要修改文件夹格式
    builder.withBucketAssigner(new DateTimeBucketAssigner[String]("yyyy-MM"))

    result.filter(_!=null).map(evaluateReport => evaluateReport.toString).addSink(streamingFileSink) //实现value中的数据写入到streamingFileSink
    environment.execute("RealTimeComputingJob")
  }

}
class MyMapFunction extends RichMapFunction[(String,String),EvaluateReport]{
  //历史数据
  var historyDataState:ValueState[HistoryData]=_
  var evaluateReportState:MapState[String,String] = _


  override def open(parameters: Configuration): Unit = {

    val valueStateDescriptor:ValueStateDescriptor[HistoryData] = new ValueStateDescriptor[HistoryData]("hds",createTypeInformation[HistoryData])
    historyDataState = getRuntimeContext.getState(valueStateDescriptor)

    val mapStateDescriptor:MapStateDescriptor[String,String] = new MapStateDescriptor[String,String]("ers",createTypeInformation[String],createTypeInformation[String])
    mapStateDescriptor.setQueryable("evaluateReportState")

    //开启TTL
//    val ttlConfig: StateTtlConfig = StateTtlConfig.newBuilder(Time.minutes(10)).build()
//    mapStateDescriptor.enableTimeToLive(ttlConfig)

    evaluateReportState = getRuntimeContext.getMapState(mapStateDescriptor)
  }

  /**
   * 每接收一次日志就执行一次这个方法
   *
   * @param in
   * @return
   */
  override def map(in: (String, String)): EvaluateReport = {
    val log:String = in._2
    var historyData: HistoryData = historyDataState.value()
    if (EvaluateUtil.isEvaluateLog(log)){
      //获取历史数据对象，获取评估报告对象，获取评估报告模板，执行评估链
      val evaluateData: EvaluateData = EvaluateUtil.parseLog2EvaluateData(log)

      //创建一个评估报告模板
      val evaluateReport: EvaluateReport = new EvaluateReport(evaluateData.getTime, evaluateData.getAppName, evaluateData.getCity, evaluateData.getGeoPoint.getLongitude + "," + evaluateData.getGeoPoint.getLatitude)

      val list: util.ArrayList[AbstractEvaluate] = new util.ArrayList[AbstractEvaluate]
      list.add(new CityEvaluate)
      list.add(new DeviceEvaluate)
      list.add(new HabitEvaluate(100))
      list.add(new InputFeaturesEvaluate)
      list.add(new PasswordEvaluate(0.95))
      list.add(new SpeedEvaluate(750))
      val evaluateChain: EvaluateChain = new EvaluateChain(list)

      evaluateChain.doEvaluate(evaluateData, historyData, evaluateReport)

      //1.把evaluateReport转换成json串
      //2获取到应用名和uuid
      //3.放入到状态里
      val json: String = new ObjectMapper().writeValueAsString(evaluateReport)
      val key: String = evaluateData.getAppName + ":" + evaluateData.getUuid
      evaluateReportState.put(key,json)

      evaluateReport
    }else{

      //登录成功日志 获取到本次登录成功的日志的成功登录数据对象
      val loginSuccessData: LoginSuccessData = EvaluateUtil.parseLog2SuccessData(log)

      if(historyData == null){
        historyData = new HistoryData()
      }

      val list = new util.ArrayList[AbstractUpdate]
      list.add(new CityUpdate)
      list.add(new DeviceUpdate(100))
      list.add(new HabitsUpdate)
      list.add(new InputFeaturesUpdate(100))
      list.add(new PasswordUpdate)
      val updateChain = new UpdateChain(list)
      updateChain.doUpdate(loginSuccessData, historyData)
      historyData.setLastLoginSuccessGeoPoint(loginSuccessData.getGeoPoint)
      historyData.setLastLoginSuccessTime(loginSuccessData.getTime)

      //把历史数据重新写入状态里
      historyDataState.update(historyData)

      null
    }
  }
}
