package com.fan.rtc.job

import com.fan.flink.entity._
import com.fan.flink.evaluate._
import com.fan.flink.update._
import com.fan.flink.util.EvaluateUtil
import org.apache.flink.api.common.serialization.{DeserializationSchema, SimpleStringSchema}
import org.apache.flink.api.common.state.{ListState, ListStateDescriptor, MapState, MapStateDescriptor, ValueState, ValueStateDescriptor}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.flink.util.Collector
import org.apache.htrace.shaded.fasterxml.jackson.databind.ObjectMapper

import java.util
import java.util.Properties
import scala.collection.JavaConverters._
object RealTimeComputingJobWithProcess {
  def main(args: Array[String]): Unit = {
    val environment: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    //模拟数据:通过socket完成
//    val log: DataStream[String] = environment.socketTextStream("hadoop10", 9999)

    //需要从kafka读取数据信息
    //    val flinkKafkaConsume:FlinkKafkaConsumer = new FlinkKafkaConsumer[](topic,valueDeserializer,properties)
    val topic = "topica"
    val properties:Properties = new Properties()
    properties.setProperty("bootstrap.servers","hadoop10:9092")

    //普通的反序列化，就是把kafka中的字节数组转换成utf-8字符串
    val valueDeserializer:DeserializationSchema[String] = new SimpleStringSchema()
    val flinkKafkaConsume:FlinkKafkaConsumer[String] = new FlinkKafkaConsumer[String](topic,valueDeserializer,properties)

    val log: DataStream[String] = environment.addSource(flinkKafkaConsume)
    //应该对log进行过滤，log必须是登录成功日志或者评估日志
    val dataStream: DataStream[String] = log.filter(log => EvaluateUtil.isEvaluateLog(log) || EvaluateUtil.isLoginSuccessLog(log))

    val keyedStream: KeyedStream[(String, String), String] = dataStream
      //把dataStream映射成(应用名:用户名,log)的数据结构
      .map(log => (EvaluateUtil.parseLog2AppNameAndUsername(log), log))
      .keyBy(_._1)//基于应用名:用户名进行keyBy处理

    val result: DataStream[EvaluateReport] = keyedStream.process(new MyKeyedProcessFunction)


    //评估报告输出：模拟：打印；最后是需要写入hdfs
//    result.filter(_!=null).print()

    environment.execute("RealTimeComputingJob")
  }

}
class MyKeyedProcessFunction extends KeyedProcessFunction[String,(String, String),EvaluateReport]{
  //valueState，用来存储历史数据的
  var historyDataState:ValueState[HistoryData]=_

  //评估报告状态，key是应用名:用户名，value是评估报告对象对应的json串
  var evaluateReportState:MapState[String,String]=_

  //用来存储可查询的状态的key（appName+":"+uuid）
  var listState:ListState[String]=_



  override def open(parameters: Configuration): Unit = {
    val valueStateDescriptor: ValueStateDescriptor[HistoryData] = new ValueStateDescriptor[HistoryData]("hds",createTypeInformation[HistoryData])

    historyDataState=getRuntimeContext.getState(valueStateDescriptor)

    val mapStateDescriptor: MapStateDescriptor[String, String] = new MapStateDescriptor[String,String]("ers",createTypeInformation[String],createTypeInformation[String])
    mapStateDescriptor.setQueryable("evaluateReportState")
    evaluateReportState=getRuntimeContext.getMapState(mapStateDescriptor)


    listState=getRuntimeContext.getListState(new ListStateDescriptor[String]("lsd",createTypeInformation[String]))
  }

  override def onTimer(timestamp: Long, ctx: KeyedProcessFunction[String, (String, String), EvaluateReport]#OnTimerContext, out: Collector[EvaluateReport]): Unit = {

    //把状态中的数据删除掉：evaluateReportState状态中的数据删除掉
    //    evaluateReportState.clear()//这个是有问题的，把一个应用下的一个用户的评估报告，一次性全部删除掉啦

    //从listState里面取出来最前面的那个
    val list: scala.List[String] = listState.get().asScala.toList
    evaluateReportState.remove(list(0))

    val javaList: util.List[String] = list.asJava
    val first: String = javaList.get(0)
    val list2: scala.List[String] = list.filter(e => !(e.equals(first)))

    listState.update(list2.asJava)

  }

  override def processElement(i: (String, String), context: KeyedProcessFunction[String, (String, String), EvaluateReport]#Context, collector: Collector[EvaluateReport]): Unit = {
    val log: String = i._2
    var historyData: HistoryData = historyDataState.value() //获取到历史数据
    if (EvaluateUtil.isEvaluateLog(log)) {
      //获取到历史数据对象，获取到评估数据对象，获取到评估报告模板。执行评估链
      val evaluateData: EvaluateData = EvaluateUtil.parseLog2EvaluateData(log)

      //创建一个评估报告模板
      val evaluateReport: EvaluateReport = new EvaluateReport(evaluateData.getTime, evaluateData.getAppName, evaluateData.getCity, evaluateData.getGeoPoint.getLongitude + "," + evaluateData.getGeoPoint.getLatitude)

      val list: util.List[AbstractEvaluate] = new util.ArrayList[AbstractEvaluate]
      list.add(new CityEvaluate)
      list.add(new DeviceEvaluate)
      list.add(new HabitEvaluate(1))
      list.add(new InputFeaturesEvaluate)
      list.add(new PasswordEvaluate(0.95))
      list.add(new SpeedEvaluate(750))
      val evaluateChain: EvaluateChain = new EvaluateChain(list)

      evaluateChain.doEvaluate(evaluateData, historyData, evaluateReport)

      val json: String = new ObjectMapper().writeValueAsString(evaluateReport)
      val key: String = evaluateData.getAppName + ":" + evaluateData.getUuid
      evaluateReportState.put(key, json)
      //在往状态里面放的时候，需要设置一个定时器：注册一个时间，达到了这个时间之后，就会执行onTimer方法
      //5分钟后执行onTimer
      val timer = context.timerService.currentProcessingTime + 5 * 1000L //5秒钟
      context.timerService.registerProcessingTimeTimer(timer)
      listState.add(key)

      collector.collect(evaluateReport)
    } else {
      //登录成功日志

      //获取到本次成功登录的日志对应的成功登录数据对象
      val loginSuccessData: LoginSuccessData = EvaluateUtil.parseLog2SuccessData(log)

      if (historyData == null) {
        historyData = new HistoryData()
      }
      val list = new util.ArrayList[AbstractUpdate]
      list.add(new CityUpdate)
      list.add(new DeviceUpdate(100))
      list.add(new HabitsUpdate)
      list.add(new InputFeaturesUpdate(100))
      list.add(new PasswordUpdate)
      val updateChain = new UpdateChain(list)

      updateChain.doUpdate(loginSuccessData, historyData)

      historyData.setLastLoginSuccessGeoPoint(loginSuccessData.getGeoPoint)
      historyData.setLastLoginSuccessTime(loginSuccessData.getTime)

      //把历史数据重新写入到状态里面
      historyDataState.update(historyData)

    }
  }
}