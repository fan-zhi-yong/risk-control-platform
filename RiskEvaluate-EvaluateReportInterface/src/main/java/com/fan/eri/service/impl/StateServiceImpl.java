package com.fan.eri.service.impl;

import com.fan.eri.service.StateService;
import org.apache.flink.api.common.JobID;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.queryablestate.client.QueryableStateClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.concurrent.CompletableFuture;

@Service
public class StateServiceImpl implements StateService {
    @Value("${flink.state.remoteHost}")
    private String remoteHostname;
    @Value("${flink.state.remotePort}")
    private int remotePort;
    @Value("${flink.state.jobId}")
    private String jobId;
    @Value("${flink.state.queryableStateName}")
    private String queryableStateName;
    @Override
    public String queryEvaluateReport(String appName, String username, String uuid) {
        //通过QueryableStateClient提供的方法完成到flink服务器里面读取状态数据
        try {
            QueryableStateClient client=new QueryableStateClient(remoteHostname,remotePort);
            JobID jobId=JobID.fromHexString(this.jobId);
            String key=appName+":"+username;
            TypeInformation<String> keyTypeInfo=TypeInformation.of(String.class);
            MapStateDescriptor<String,String> mapStateDescriptor = new MapStateDescriptor<String, String>("msd",TypeInformation.of(String.class),TypeInformation.of(String.class));
            CompletableFuture<MapState<String, String>> future = client.getKvState(jobId, queryableStateName, key, keyTypeInfo, mapStateDescriptor);
            MapState<String, String> mapState = future.get();
            return mapState.get(appName+":"+uuid);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("发送了一些问题");
        }

    }
}
