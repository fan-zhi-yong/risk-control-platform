package com.fan.eri.service;

import org.springframework.stereotype.Service;

@Service
public interface StateService {
    String queryEvaluateReport(String appName, String username, String uuid);
}
