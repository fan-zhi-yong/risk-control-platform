package com.fan.eri.controller;

import com.fan.eri.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StateController {
    @Autowired
    private StateService stateService;
    @RequestMapping("/queryState/{apiId}/{apiKey}/{appName}/{username}/{uuid}")
    public String queryState(@PathVariable("apiId") String apiId,
                             @PathVariable("apiKey") String apiKey,
                             @PathVariable("appName") String appName,
                             @PathVariable("username") String username,
                             @PathVariable("uuid") String uuid){
        return stateService.queryEvaluateReport(appName,username,uuid);
    }
}
